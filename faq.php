<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>FAQ</title>

<?php
$page="faq";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Frequently Asked Questions</h1>
                </header> 
                <div class="page-content">
                    <div class="row page-row">
                        <div class="faq-wrapper col-md-8 col-sm-7">                         
                            <div class="panel-group" id="accordion">

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-8">
                                            <b>Do you offer a part-time Montessori program, 2-3 days a week or less?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        We do not offer a part-time program. The five-day program provides a consistent, predictable environment which frees children from having to repeatedly readjust to a changing schedule, and provides them with the best space in which to learn and grow.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-2">
                                            <b>Who is eligible for the full day program?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        Students may be considered eligible for this program when their teacher in collaboration with the school director determines their readiness and the following criteria can be met. Students can be admitted into this program at any time during the school year.
                                        
                                        <h5>Returning Students</h5>
                                        <ol>
                                            <li>age (how close their 5th birthday is to September 1 of the current school year)</li>
                                            <li>social and emotional maturity</li>
                                            <li>academically prepared for the Primary All-Day Program </li>
                                            <li>no longer naps in the afternoon</li>
                                            <li>interest of the student</li>
                                            <li>has good working cycles</li>
                                            <li>has reached a degree of independence</li>
                                        </ol>

                                        <h5>New Students</h5>
                                        <p>Before new students may be considered eligible for the Primary All-Day Program they need a period of time in the classroom in which they can become familiar with the Montessori equipment and classroom.  They must also meet the above criteria.</p>

                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-3">
                                            <b>How much parent involvement is expected?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        There is no <em>required</em> parent involvement. You are encouraged to participate in all school functions. There are several opportunities to connect with the teachers, staff and other parents throughout the year.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->


                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-4">
                                            <b>Do you offer Before Care and After Care?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        Yes both are offered. Please refer to the <a href="programs.php#bac">Programs Page</a>
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-5">
                                            <b>Is the school bilingual? It's called "Casa" so do you teach in Spanish?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        No, the school is not bilingual. Casa de Montessori means House of Montessori in Spanish.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->


                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-6">
                                            <b>Does my child need to be potty trained to start at the school?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        Yes, the children do need to be able to go to the bathroom independently.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->                                                                

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-7">
                                            <b>Why do Montessori classes group different age levels together?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        Montessori classes are organized to encompass a three-year age span, which allows younger students the stimulation of older children, who in turn benefit from serving as role models. Each child learns at her own pace and will be ready for any given lesson in her own time. In a mixed-age class, children can always find peers who are working at their current level. Furthermore, working in one classroom allows students to develop a strong sense of community with their classmates and teachers.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-9">
                                            <b>How many teachers per child?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-9" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        We have two adults – one directress and one assistant -- per classroom. Classes have a maximum of 25 children. In addition we have floating assistants who help at lunchtime, recess and with taking children to the bathroom. We invite you to visit to observe our program in action (<a href="contact.php">contact us</a> to set up a time).
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->  

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-10">
                                            <b>When can I visit and do I need to make an appointment?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-10" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        Casa accepts visitors on Monday, Wednesdays and Thursdays between 9:15 and 11:00.  Observations are typically about 20 minutes followed with questions if you have them. Appointments are preferred.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->  								


                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" class="collapsed" href="#collapse-11">
                                            <b>At what age can my child be enrolled?</b>
                                            </a>
                                        </h4>
                                    </div><!--//pane-heading-->
                                    <div id="collapse-11" class="panel-collapse collapse">
                                        <div class="panel-body">
                                        The optimal age would be three years old, with a commitment of three years in order to receive the maximum benefits of the Montessori program. This includes their kindergarten year.
                                        </div><!--//panel-body-->
                                    </div><!--//panel-colapse-->
                                </div><!--//panel-->  	
								
								
                            </div><!--//panel-group-->                                                
                        </div><!--//faq-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">                    

                            <?php
                            require_once("./inc/forms.php");
                            ?>

                            <?php
                            require_once("./inc/brochure.php");
                            ?>  

                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script> 
    
</body>
</html> 