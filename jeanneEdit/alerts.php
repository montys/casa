<!--
=====================================================================================
THESE ARE THE ALERTS THAT MAY APPEAR AT THE TOP OF THE HOME PAGE OR THE PROGRAM PAGE
EDIT THE TEXT AS NEEDED, or HIDE THEM.
=====================================================================================           
-->
<?php

	//$homealert	= "SAVE THE DATE: <b>Casa de Montessori Open House</b>: January 23rd, 2016 from 10am - 2pm";
	$progalert	= "We are now ACCEPTING APPLICATIONS for the 2018/2019 academic year.";
	//$homealert	= "<b>NOTE: TUESDAY, FEB 8TH - NO BEFORE CARE.  School will start at 8:45.</b>";
	//$homealert	= "<b>NOTE:</b> Casa follows Montgomery County Public Schools' bad weather policy. Please <a href='http://www.montgomeryschoolsmd.org/emergency/alertmcps.aspx'>sign up for alerts</a> now to stay informed.";
	//$homealert	= "<b>THURSDAY, MARCH 22, 2018 - Casa de Montessori will OPEN at 8:45.  NO BEFORE CARE available. </b>";
	//$homealert	= "<b>Now accepting applications for the 2017/2018 academic year.  Please call for information about our programs or to schedule a tour. ";

	// ==================================================================================
	// DO NOT EDIT BELOW THIS LINE
	// ==================================================================================
	function checkAlert($msg) {
		$o="";
		if (isset($msg) && $msg != "") {
			$o .= ('<section class="promo alert alert-info">');
			$o .= ('<i class="fa fa-exclamation-circle"></i> ' . $msg);
			$o .= ('</section>');
			print $o;
		}
	}

?>