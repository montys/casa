var map;
jQuery(document).ready(function(){

    map = new GMaps({
        div: '#map',
        lat: 39.086067,
        lng: -76.998548,
    });
    map.addMarker({
        lat: 39.086067,
        lng: -76.998548,
        title: 'Address',      
        infoWindow: {
            content: '<h5 class="title">Casa de Montessori</h5><p><span class="region">14015 New Hampshire Ave, Silver Spring</span></p><img src="assets/images/misc/map.png">'
        }
        
    });


});
