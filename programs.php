<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Programs</title>

<?php
$page="programs";
require_once("./jeanneEdit/alerts.php");
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Our Programs</h1>
                </header> 
                <div class="page-content">
                    <div class="row page-row">
                        <div class="course-wrapper col-md-8 col-sm-7">                         
                            <article class="course-item">

																<?php
                                  checkAlert($programalert);						
                                ?>
                            
                                <p class="featured-image page-row"><img class="img-responsive" src="assets/images/courses/course-1.jpg" alt=""/></p>

                                <div class="page-row">
                                    <p>The foundation of the Montessori experience, the primary program, is for children in the important early formative years. Beginning with practical and social skills, children learn to keep track of their belongings, to put things away, to share materials. Academics are introduced through concrete, manipulative materials which utilize all five senses and lay the groundwork for abstract thinking.</p>

                                    <p><b>Casa follows the Montessori philosophy of grouping together children 2½ to 6 years of age in a five day a week program.</b></p>

                                    <p>Casa has two classrooms with 25 children of mixed ages in each classroom. Each classroom has an AMI trained directress and an assistant. There are two additional assistants available to make materials, take children to the bathroom and help out where needed.</p>
            
                                    <p>Lessons are given to children individually, in small groups and to the class as a whole throughout the morning. The children may work in any area in which they have been given a lesson. They may work independently or with other children as their interest leads them or as they are directed.</p>

                                    <br />

                                    <div class="panel panel-theme">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><b>Primary</b></h3>
                                            
                                        </div>
                                        <div class="panel-body">
                                            <ul class="list-group">
                                                <li class="list-group-item"><strong>School Day:</strong> 8:45 am - 12:00pm (before and after care also available)</li>
                                                <li class="list-group-item"><strong>Ages:</strong> 2½ to 4 years old</li>
                                                <li class="list-group-item"><strong>Tuition:</strong> $6,000</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel panel-theme">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><b>Primary All Day</b></h3>
                                            
                                        </div>
                                        <div class="panel-body">
                                            <p>Children are admitted according to <a href="faq.php">eligibility criteria</a> and in collaboration with the teachers and the school director. A 4-year old child may be eligible for this program.</p>
                                            <ul class="list-group">
                                                <li class="list-group-item"><strong>School Day:</strong> 8:45 am - 3:00pm (before and after care also available)</li>
                                                <li class="list-group-item"><strong>Ages:</strong> 4 years old</li>
                                                <li class="list-group-item"><strong>Tuition:</strong> $8,600</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel panel-theme">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><b>Kindergarten</b></h3>
                                            
                                        </div>
                                        <div class="panel-body">                                     
                                            <p>Child must be 5 years old by September 1 of the school year enrolled.</p>
                                            <ul class="list-group">
                                                <li class="list-group-item"><strong>School Day:</strong> 8:45 am - 3:00pm (before and after care also available)</li>
                                                <li class="list-group-item"><strong>Ages:</strong> 5 years old</li>
                                                <li class="list-group-item"><strong>Tuition:</strong> $8,600</li>
                                            </ul>
                                        </div>
                                    </div>
 
                                    <br/>

                                    <h2 id="bac">Before/After School Care</h2>

                                    <p><strong>7:30am-8:45am and Noon to 6.00pm</strong></p>

                                    <p>Only those children attending Casa’s Montessori programs may use the Before and After School Care program.
                                    <p>Fees are calculated according to the number of days and hours used, to the nearest 5 minutes. Fees are calculated separately for AM and PM.</p>
                                    <p>Late Pick up after 12:05pm or 6:05pm = $3 per minute payable at time of pick up. A full day is charged (7:30am - 6:00pm) when the responsible adult fails to sign a child in or out.</p>

                                </div><!--//page-row-->
                                <div class="tabbed-info page-row">             
                                    <ul class="nav nav-tabs">
                                      <li class="active"><a href="#tab1" data-toggle="tab">Regular Use</a></li>
                                      <li><a href="#tab2" data-toggle="tab">Drop In or Occasional Use</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="tab1">
                                            <p>If you consistantly use:</p>
                                        <div class="table-responsive">  
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Use</th>
                                                        <th>Rates</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5 days per week</td>
                                                        <td>$6.00 /hour</td>
                                                    </tr>
                                                    <tr>
                                                        <td>4 days per week</td>
                                                        <td>$6.25 /hour</td>
                                                    </tr>
                                                    <tr>
                                                        <td>3 days per week</td>
                                                        <td>$6.50 /hour</td>
                                                    </tr>
                                                    <tr>
                                                        <td>2 days per week</td>
                                                        <td>$6.75 /hour</td>
                                                    </tr>
                                                    <tr>
                                                        <td>1 days per week</td>
                                                        <td>$8.50 /hour</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div><!--//table-responsive-->
                                        </div>
                                        <div class="tab-pane" id="tab2">
                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Use</th>
                                                        <th>Rates</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Drop in Care or Occasional Use</td>
                                                        <td>$10.00 /hour</td>
                                                    </tr>
                                                </tbody>
                                            </table>                                        
                                        </div>
                                    </div>

                                    <p>Casa has a spacious room for before and after school care across the hall from our classrooms. There are a wide variety of books, puzzles, games, and art and craft materials. We have trucks, balls, dolls, play dishes, a doll house, sports equipment, building apparatus, etc.</p>
                                    <p>The pre-school children using after school care bring their lunches and eat in this room. After lunch, they have an outside recess for about three quarters of an hour, then return inside to nap or rest quietly while a story is read to them.</p>

                                </div><!--//tabbed-info-->                   
                            </article><!--//course-item-->                                              
                        </div><!--//course-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">     


                            <?php
                            require_once("./inc/referral.php");
                            ?>
                            
                            <?php
                            require_once("./inc/humanTendancies.php");
                            ?>  

                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script> 
    
</body>
</html> 

