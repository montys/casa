<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>About Us..</title>
    
<?php
$page="about";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>
    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">About Us</h1>
                </header> 
                <div class="page-content">                 
                    <div class="row page-row">                     
                        <div class="team-wrapper col-md-8 col-sm-7">        
                            <div class="row page-row" >
                                <p>Casa de Montessori offers an enlightening and nurturing Montessori environment to educate and support each individual child in building a foundation for life-long learning.</p>
                            </div>
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-burkhart.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Betty Burkhardt</h3>
                                    <h4>Principal/Owner</h4>
                                    <p>Although I was born in Cuba, of American parents, I have been a lifelong resident of this area. I received my degree from the University of Maryland in English and Psychology. I completed my Montessori Primary degree in 1974 and began teaching the same year. My love and interest in the development of children has only increased over time.</p>
                                    <p>My neighbor and friend (Judy Fallon) for whom I worked established Casa de Montessori in 1969 and I took the title of Directress of Casa in 1980.</p>
                                    <p>Two of my six children and six of my eight grandchildren attended Casa.</p>
                                </div>                               
                            </div>
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-jones.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Luisa Jones</h3>
                                    <h4>Primary Montessori Directress</h4>
                                    <p>I was born in Italy and came to the United States in 1986. I didn’t know then that this was going to be my permanent home but I have always known that young children were going to be a big part of my future.</p>
                                    <p>Once I was introduced to the Montessori world I had no doubts that becoming a Montessori teacher was my purpose in life. Casa de Montessori opened the doors to my practice teaching in the spring of 1995 and once I graduated later that year, handed me the precious key to my classroom. It is such a privilege to observe young minds at work and be instrumental in offering a child the knowledge that will awaken their most personal interest. I wish I could have been in a Montessori classroom as a child myself, however I feel extremely fortunate to experience it each day as an adult.</p>
                                    <p>The difference lies in the level of consciousness: children are not always aware of the great discoveries they’re making as they try to decode their physical and social environment. On the contrary, the keen eye of the Montessori teacher sees what they have mastered and all their accomplishments even in their simplest forms.</p>
                                    <p>My reason and motivation to continue teaching is each child’s success and the positive experience they receive during their very first school year.</p>
                                </div>                               
                            </div>
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-lackey.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Katherine Howser</h3>
                                    <h4>Primary Montessori Directress</h4>
                                    <p>I received my degree from Oklahoma State University with a major in Elementary Education.  In 2014, I graduated from Loyola University with my Primary Montessori Certification and my Masters in Education.  I am a lover of animals and have raised all types.  I am married and have 3 cats and a new puppy, Indy.  I am thrilled to be part of the Casa Family.</p>
                                </div>                               
                            </div>
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-nelson.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Sianne Nelson</h3>
                                    <h4>Assistant in Mrs. Jones' class</h4>
                                    <p>I was born and raised in Indonesia. I completed my college education there. I worked for a company in their accounting department for several years in Indonesia before I moved to the United States in December 1999.</p>
                                    <p>I lived in Sacramento, California and Memphis, Tennessee before I moved to Silver Spring, Maryland in October 2003.</p>
                                    <p>I had an opportunity to work with Casa through one of the families whose child attended Casa de Montessori. The school needed an aide in the classroom. I was so pleased to be offered the position and have been happily working at Casa since 2008 with Mrs. Jones in her classroom.</p>
                                </div>                               
                            </div>                            
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-lupe.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Señora Guadalupe Robles</h3>
                                    <h4>Before and After Care Senior Staff</h4>
                                  <p>I came to know about Casa through a good friend who had signed up her daughter at Casa. She told me they were looking for a native Spanish speaking speaker to teach Spanish. She thought I would be a perfect candidate so I came to see the school.</p>
                                    <p>I was interviewed by Mrs. Burkhardt and after we finished she asked me, "So when can you start?"</p>
                                    <p>This was in September 1994 and since then I have been with Casa. I enjoy being with the children and getting to know their individual personalities. I learn so much from them and the staff at Casa makes me feel like I am part of a big caring family.</p>
                                    <p>I enjoy passing my knowledge to the children and I am so proud to see how they each grow and learn, and use what they are being taught. I couldn’t have asked for a better opportunity to teach and learn from such smart students.</p>
                                </div>                               
                            </div>
                            <div class="row page-row" >
                                <figure class="thumb col-md-3 col-sm-4 col-xs-6">
                                    <img class="img-responsive" src="assets/images/team/team-bentley.jpg" alt="" />
                                </figure>
                                <div class="details col-md-9 col-sm-8 col-xs-6">
                                    <h3 class="title">Jeanne Bentley</h3>
                                    <h4>Administrator</h4>
                                    <p>I was born and raised in Silver Spring, Maryland by my wonderful parents. I have 5 brothers. My younger brother and I attended Casa de Montessori. My mother was an aide there at the time. She has since become a Primary Directress and eventually the Principle of the school.</p>
                                    <p>I received my degree in Mathematics and Computer Science from the University of Delaware. I worked for many years as a contractor at NASA/Goddard Space Flight Center as a Network Administrator. After having my first two children whom attended Casa, I came to work here. I worked first as an Assistant and a few years later, I became an Aide in the classroom. I have moved in to my current position as Administrator this past year. I currently live on Kent Island with my husband and three children.</p>
                                </div>                               
                            </div>                            
                            
                        </div><!--//team-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">      

                            <?php
                            require_once("./inc/amicert.php");
                            ?>

                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->


<?php
require_once("./inc/footer.php");
?>
 
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>            


</body>
</html> 