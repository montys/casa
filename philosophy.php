<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Montessori Philosophy</title>
    
<?php
$page="philosophy";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>
    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Montessori Philosophy</h1>
								</header>

                <div class="page-content">                 
                    <div class="row page-row">                     
                        <div class="team-wrapper col-md-8 col-sm-7">        
                        
													<p>Maria Montessori identified 10 human tendencies which correspond to the 10 universal needs of mankind.The child becomes the creator of his own world by satisfying his natural instincts from Orientation to Self-perfection, which are the foundation of Montessori Education.</p>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-orientation.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Orientation</h3>
                                  <p>"I am happy when I belong."</p>
                                  <p>Human beings want to know their relationship to the environment around them. When children enter a new environment, they often want to look at and touch everything around them. They enjoy knowing "where" they fit in - from learning their address to finding their country and continent on a map.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-exploration.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Exploration</h3>
                                  <p>"I discover the world with all my senses."</p>
                                  <p>Our earth is filled with wonderful sounds, scents, textures, tastes, and colors. Children are naturally curious, and love to use their senses to learn more about the fascinating world around them.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-comm.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Communication</h3>
                                  <p>"I express myself in many ways."</p>
                                  <p>Humans delight in conveying thoughts, feelings, and information to each other. Various types of communication include the written and spoken word, touch, facial expressions, gestures, art, music, and dance. Communication is the link of understanding between people, both face-to-face and from generation to generation.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-movement.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Movement</h3>
                                  <p>"Through movement I reach out and take in"</p>
                                  <p>People generally like to stay busy. For children, movement can be enjoyed for its own sake, rather than always having a goal or end product in mind. Even children who have very little to play with will find ways to be active through games, songs, dance, and pretend play.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-manipulation.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Manipulation/Work</h3>
                                  <p>"My hand is the instrument of my mind."</p>
                                  <p>Humans need to take hold of their environment to understand it. It is the next step after exploration: once you have found something interesting, you will quite naturally want to use it in some way.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-order.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Order</h3>
                                  <p>"Internal and External order make me see things clearly."</p>
                                  <p>People prefer order to chaos and confusion. Order brings predictability and security. There are two kinds of order: external and internal. An orderly classroom (external) helps children to have orderly thoughts (internal).</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-repetition.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Repetition</h3>
                                  <p>"As I repeat I build up confidence"</p>
                                  <p>This occurs when a child repeats a task over and over again. Oftentimes it is with the intent to master the task, but even after mastery occurs, a child may continue to repeat the activity for the sheer pleasure of doing so.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-exactness.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Exactness</h3>
                                  <p>"I draw attention to details."</p>
                                  <p>Have you ever seen a child get upset because something was put back in the wrong place? Or watched them line up their blocks neatly before building a tower? Instinctively, humans seek to be precise in their work. Doing something exactly right brings enormous satisfaction.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-abstraction.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Abstraction</h3>
                                  <p>"I realize my ideas"</p>
                                  <p>This is truly the characteristic that sets us apart from animals. We are able to visualize events that have not yet occurred; we are able to feel and express emotions that are not tangible. We can imagine something that exists only in our minds, and then take the steps to make it happen.</p>
                                </div>                               
                            </div>

                            <div class="row page-row" >
                                <figure class="thumb col-md-4 col-sm-4 col-xs-6">
                                    <br ><img class="img-responsive" src="assets/images/phil/phil-self-perfection.jpg" alt="" />
                              </figure>
                                <div class="details col-md-8 col-sm-8 col-xs-6">
                                  <h3 class="big">Self-Perfection</h3>
                                  <p>"I did it myself!"</p>
                                  <p>All of the tendencies culminate in this one. Once we have explored, manipulated, and worked in our environment, we can perfect our activities. In doing so, we are masters of our own minds and bodies as well as the tasks we set out to do.</p>
                                </div>                               
                            </div>
                            
                        </div><!--//team-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">      

                            <?php
                            require_once("./inc/amicert.php");
                            ?>

                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->


<?php
require_once("./inc/footer.php");
?>
 
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>            


</body>
</html> 