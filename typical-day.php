<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>A typical day</title>

<?php
$page="typical";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">A typical day</h1>
                </header> 
                <div class="page-content">  
                    <div class="row page-row">
                        <div class="gallery-wrapper">
                            <div class="page-row">
                                <p>Casa de Montessori strives to provide a structured educational environment where children are carefully nurtured so that they are able to develop to their fullest potential.</p>
                            </div>
                            <div class="row page-row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="album-cover">
                                        <img class="img-responsive" src="assets/images/gallery/casa-2.jpg" alt="" />
                                        <div class="desc">
                                            <h4><small><a href="#">Morning</a></small></h4>
                                            <p>Lessons are given to children individually, in small groups, and to the class as a whole. The children may work in any of the Montessori areas in which they have been given a lesson.</p>
                                            <p>They work independently or with other children as their interest leads them or as they are directed. Art and music are also a part of their day.</p>
                                            <p>A snack is available during the morning hours. Weather permitting; there is a short recess time before the noon dismissal.</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="album-cover">
                                        <img class="img-responsive" src="assets/images/gallery/casa-1a.jpg" alt="" />
                                        <div class="desc">
                                          <h4><small><a href="#">Lunch Time</a></small></h4>
                                            <p>All children staying past noon bring their lunches. Microwaves are available to warm food.</p>
                                            <p>The primary all-day and kindergarten children eat in their classrooms using glass dishes with cloth placemats and napkins. They learn to set up their meal and after eating they clean up the table and wash their dishes.</p>
                                            <p>Children in after care are encouraged to develop independence by caring for themselves.</p>
                                            <p>After lunch all children take recess, either outside or in the Before/After Care room.</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="album-cover">
                                        <img class="img-responsive" src="assets/images/gallery/casa-3.jpg" alt="" />
                                        <div class="desc">
                                            <h4><small><a href="#">Afternoon</a></small></h4>
                                            <p>During the afternoon the primary all-day and kindergarten children continue to work in all areas of the classroom as they further develop their skills of writing, listening, speaking, comprehension and memorization.</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="album-cover">
                                        <img class="img-responsive" src="assets/images/gallery/casa-4.jpg" alt="" />
                                        <div class="desc">
                                            <h4><small><a href="#">Aftercare naptime</a></small></h4>
                                            <p>After recess the children using after school care take a nap or rest quietly on a cot while a story is read to them. They may also take books to their cots to look at quietly.</p>
                                            <p>Weather permitting, all children staying after 3:00 who are not asleep go outside to play. About 4:00 a snack is served. The remainder of their day is spent with individual or small group activities.</p>
                                        </div>
                                    </div>
                                </div>
                            
                            </div><!--//page-row-->
                        </div><!--//gallery-wrapper-->
                    </div>
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->


<?php
require_once("./inc/footer.php");
?>  

    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>    
    
</body>
</html> 

