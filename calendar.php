<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Calendar</title>

<?php
$page="calendar";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>
    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Calendar for the School Year</h1>
                </header> 
                <div class="page-content">
                    <div class="row page-row">
                        <div class="events-wrapper col-md-8 col-sm-7">                         
                            
                            <div class="responsive-iframe-container">
                            	<iframe src="https://www.google.com/calendar/embed?showTitle=0&amp;showPrint=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;src=9198q753g8nbagqiqsgs4nqf74%40group.calendar.google.com&amp;color=%235B123B&amp;ctz=America%2FNew_York" style=" border-width:0 " width="800" height="600" frameborder="0" scrolling="no"></iframe>
														</div>
                            
                            <!--article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">AUG</span>
                                        <span class="date-number">24</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Parent Orientation</h3>
                                    <p class="meta"><span class="time"><i class="fa fa-clock-o"></i>7:00pm - 8:00pm</span></p>  
                                    <p class="desc">New and existing parents are welcomed to the school. Schedules are explained and questions answered.</p>                       
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">AUG</span>
                                        <span class="date-number">26-28</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Open House for New Students</h3>
                                    <p class="meta"><span class="time"><i class="fa fa-clock-o"></i>9:00am - 12:00pm</span></p>  
                                    <p class="desc">School opens for new students only.</p>                       
                                </div>
                            </article> 
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">AUG</span>
                                        <span class="date-number">31</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">First Day of School</h3>
                                    <p class="desc">Before and after care available.</p>                       
                                </div>
                            </article>

                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">SEP</span>
                                        <span class="date-number">7</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">No School</h3>
                                    <p class="desc">Labor Day Holiday.</p>  
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">SEP</span>
                                        <span class="date-number">25</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Noon Closure</h3>
                                    <p class="desc">Early release day - Staff Meeting.</p>                       
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">SEP</span>
                                        <span class="date-number">26</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Parent Dinner</h3>
                                    <p class="meta"><span class="time"><i class="fa fa-clock-o"></i>7:00pm - 9:00pm</span></p>  
                                    <p class="desc">Adult only function.</p>                       
                                </div>
                            </article>

                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">Oct</span>
                                        <span class="date-number">30</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Noon Closure</h3>
                                    <p class="desc">Early release day - Staff Meeting.</p>                       
                                </div>
                            </article>

                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">NOV</span>
                                        <span class="date-number">2</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">No School</h3>
                                    <p class="desc">Staff in service day.</p>  
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">NOV</span>
                                        <span class="date-number">11</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Parent Night</h3>
                                    <p class="meta"><span class="time"><i class="fa fa-clock-o"></i>7:00pm - p:00pm</span></p>  
                                    <p class="desc">Learn about Montessori</p>                       
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">NOV</span>
                                        <span class="date-number">25</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Noon Closure</h3>
                                    <p class="desc">Early release day - staff meeting.</p>                       
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">NOV</span>
                                        <span class="date-number">26-27</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">No School</h3>
                                    <p class="desc">Thanksgiving Holiday</p> 
                                </div>
                            </article>

                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">DEC</span>
                                        <span class="date-number">4</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">No School</h3>
                                    <p class="desc">Parent-teacher conferences</p>
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">DEC</span>
                                        <span class="date-number">18</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">Noon Closure</h3>
                                    <p class="desc">Early release day - staff meeting.</p>   
                                </div>
                            </article>
                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">DEC</span>
                                        <span class="date-number">21-31</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">No School</h3>
                                    <p class="desc">Holiday</p>
                                </div>
                            </article>

                            <article class="events-item page-row has-divider clearfix">
                                <div class="date-label-wrapper col-md-1 col-sm-2">
                                    <p class="date-label">
                                        <span class="month">MONTH</span>
                                        <span class="date-number">DD</span>
                                    </p>
                                </div>
                                <div class="details col-md-11 col-sm-10">
                                    <h3 class="title">TXTXTXT</h3>
                                    <p class="meta"><span class="time"><i class="fa fa-clock-o"></i>?:00am - ?:00pm</span></p>  
                                    <p class="desc">DSCDSCDSCDSC</p>                       
                                </div>
                            </article>

                            <br><br>
                            <p class="info">MORE EVENTS WILL BE ADDED</p-->
                            
                        </div><!--//events-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">

                            <section class="widget">
                                <h3 class="title">Printable Calendar</h3>
                                <article class="news-item row">       
                                    <figure class="thumb col-md-2">
                                        <img src="assets/images/misc/cal-icon.png" alt="" >
                                    </figure>
                                    <div class="details col-md-10">
                                        <h4 class="title"><a href="assets/forms/Casa-Calendar-2019-2020.pdf">Download PDF</a></h4>
                                    </div>
                                </article><!--//news-item-->
                            </section><!--//widget-->
                            
                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>  

    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>    
    
</body>
</html> 

