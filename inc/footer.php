    
    <!-- ******FOOTER****** --> 
    <footer class="footer">
        <div class="footer-content">
            <div class="container">
                <div class="row">
                <div class="footer-col col-md-2 col-sm-3 about">
                    <div class="footer-col-inner">
                        <h3>About</h3>
                        <ul>
                            <li><a href="about.php"><i class="fa fa-caret-right"></i>About us</a></li>
                            <li><a href="contact.php"><i class="fa fa-caret-right"></i>Contact us</a></li>
                            <li><a href="apply.php"><i class="fa fa-caret-right"></i>Apply</a></li>
                            <li><a href="calendar.php"><i class="fa fa-caret-right"></i>Calendar</a></li>
                        </ul>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <div class="footer-col col-md-7 col-sm-9 newsletter">
                    <div class="footer-col-inner">
                        <h3>AMI Certification</h3>
                        <p>We have been affiliated with and held certification from the Association Montessori Internationale (AMI) since 1969, when the school was founded.</p>
                        <p>Our AMI certification and recognition means that we closely adhere to the standards and methods promoted in teacher training courses provided by AMI.</p>
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col--> 
                <div class="footer-col col-md-3 col-sm-12 contact">
                    <div class="footer-col-inner">
                        <h3>Contact us</h3>
                        <div class="row">
                            <p class="adr clearfix col-md-12 col-sm-4">
                                <i class="fa fa-map-marker pull-left"></i>        
                                <span class="adr-group pull-left">       
                                    <span class="street-address">14015 New Hampshire Ave</span><br>
                                    <span class="region">Silver Spring, MD</span><br>
                                    <span class="postal-code">20904</span><br>
                                    <span class="country-name">USA</span>
                                </span>
                            </p>
                            <p class="tel col-md-12 col-sm-4"><i class="fa fa-phone"></i>301 384-8404</p>
                            <p class="email col-md-12 col-sm-4"><i class="fa fa-envelope"></i><a href="mailto:info@casademontessori.com">send an email</a></p>  
                        </div> 
                    </div><!--//footer-col-inner-->            
                </div><!--//foooter-col-->   
                </div>   
            </div>        
        </div><!--//footer-content-->
        <div class="bottom-bar">
            <div class="container">
                <div class="row">
                    <small class="copyright col-md-6 col-sm-12 col-xs-12">Copyright @ 2012 Casa de Montessori</small>
                </div><!--//row-->
            </div><!--//container-->
        </div><!--//bottom-bar-->
    </footer><!--//footer-->
          