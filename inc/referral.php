                            <section class="widget has-divider"> 
                                <h1 class="section-heading text-highlight"><span class="line">Referral Discount</span></h1>
                                <p>When a new student enrolls as a result of your referral, you receive <b>10% of the new student’s tuition</b>, deducted at the end of the school year.</p>
                                <p class="promo-badge">
                                    <a class="RedDamask" href="#">
                                        <span class="percentage">10% <span class="off">OFF</span></span>                    
                                        <br>
                                        <span class="desc">Refer a friend</span>                  
                                    </a>
                                </p>
                            </section><!--//widget--> 