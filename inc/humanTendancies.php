                            <section class="widget row-divider">
                                <h1 class="section-heading text-highlight"><span class="line">Human Tendencies</span></h1>
                                <p>Maria Montessori identified 10 human tendencies which correspond to the 10 universal needs of mankind.The child becomes the creator of his own world by satisfying his natural instincts from Orientation to Self-perfection, which are the foundation of Montessori Education.</p>
                                <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                                    <div class="carousel-inner">
                                    
                                        <div class="item active">
                                            <blockquote class="quote">
                                                <h3 class="big">Orientation</h3>
                                                "I am happy when I belong."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Exploration</h3>
                                                "I discover the world with all my senses."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Communication</h3>
                                                "I express myself in many ways."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Movement</h3>
                                                "Through movement I reach out and take in"
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Manipulation/Work</h3>
                                                "My hand is the instrument of my mind."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Order</h3>
                                                "Internal and External order make me see things clearly."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Repetition</h3>
                                                "As I repeat I build up confidence"
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    
                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Exactness</h3>
                                                "I draw attention to details."
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        

                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Abstraction</h3>
                                                "I realize my ideas."
                                            </blockquote>                                             
                                        </div><!--//item-->                                        

                                        <div class="item">
                                            <blockquote class="quote">
                                                <h3 class="big">Self-Perfection</h3>
                                                "I did it myself!"
                                            </blockquote>                                             
                                        </div><!--//item-->
                                        
                                    </div><!--//carousel-inner-->
                                    <div class="carousel-controls">
                                        <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                                        <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i></a>
                                    </div><!--//carousel-controls-->
                                </div><!--//testimonials-carousel-->                             
                            </section><!--//widget-->