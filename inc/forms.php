                    <section  class="widget has-divider">
                        <h1 class="section-heading text-highlight"><span class="line">Quick Links (pdf)</span></h1>
                        <div class="section-content">
                            <ul class="list-unstyled">
                                <li><a href="assets/forms/BA-Care-2013.pdf"><i class="fa fa-book"></i> Before and After Care Form</a></li>
                                <li><a href="assets/forms/Health-Inventory-Form-1215.pdf"><i class="fa fa-book"></i> Health Inventory Form</a></li>
                                <li><a href="assets/forms/Maryland-Immunization-Certification-Form-DHMH-896-February-2014"><i class="fa fa-book"></i> Immunization Certification</a></li>
                                <li><a href="assets/forms/sy-2013-2014-msde-emergency-form-2-2"><i class="fa fa-book"></i> Emergency Contact Form</a></li>
                            </ul>
                        </div><!--//section-content-->
                    </section><!--//links-->