                            <section class="widget has-divider">
                                <h1 class="section-heading"><span class="line">Why Casa?</span></h3>
                                <p>Learn more about our school and programs with this concise, informative brochure (PDF).</p>
                                <a class="btn btn-theme" href="assets/forms/CasaBrochure.pdf"><i class="fa fa-download"></i>Download brochure</a>
                            </section><!--//widget-->

                            <section class="widget has-divider">
                                <h3 class="title">Enquiries</h3>
                                <p>We will be happy to answer any questions you have. Please call or send us an email.</p>
                                <p class="tel"><i class="fa fa-phone"></i>Tel: 301 384-8404</p>
                                <p class="email"><i class="fa fa-envelope"></i>Email: <a href="mailto:info@casademontessori.com">info@casademontessori.com</a></p>
                            </section><!--//widget-->                              