                            <section class="widget has-divider">
                                <img class="img-responsive has-divider" src="assets/images/AMI-Logo.jpg">
                                <br>
                                <h2 class="title">AMI CERTIFICATION</h2>
                                <p>Each classroom has an AMI trained directress (teacher) with an assistant. There are two additional assistants available on a daily basis, one part-time assistant and a school secretary. All staff have CPR and First Aid training and are approved by the Maryland State Department of Education and the Office of Child Care.</p>
                                <p>We have been affiliated with and held AMI certification from the <a href="http://amiusa.org/">Association Montessori Internationale</a> (AMI) since 1969, when the school was founded.</p>
                                <p>Our AMI certification and recognition means that we closely adhere to the standards and methods promoted in teacher training courses provided by AMI.</p>
                                <p>Our school is honored to have been chosen to host student teachers from local AMI training centers and student teaching from Loyola University, Maryland.</p>
                            </section><!--//widget-->
