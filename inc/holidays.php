
                    <section class="links">
                        <h1 class="section-heading text-highlight"><span class="line">Holidays/Closures</span></h1>
                        <div class="section-content">
                            <p>There is no school or before/after care on the following days:</p>
                            <ul class="list-unstyled">
                                <li><i class="fa fa-caret-right"></i> Labor Day</li>
                                <li><i class="fa fa-caret-right"></i> Martin Luther King, Jr Day</li>
                                <li><i class="fa fa-caret-right"></i> Rosh Hashanah</li>
                                <li><i class="fa fa-caret-right"></i> Presidents Day</li>
                                <li><i class="fa fa-caret-right"></i> Thanksgiving 2 Days</li>
                                <li><i class="fa fa-caret-right"></i> Spring Break (2 weeks)</li>
                                <li><i class="fa fa-caret-right"></i> Winter Break (9 days)</li>
                                <li><i class="fa fa-caret-right"></i> Memorial Day</li>
                                <li><i class="fa fa-caret-right"></i> Staff in Service (2 Days)</li>
                                <li><i class="fa fa-caret-right"></i> Parent Conference Day</li>
                                <li><i class="fa fa-caret-right"></i> Staff Meetings (6 half days)</li>
                            </ul>  
                            <a class="read-more" href="calendar.php">Full Calendar<i class="fa fa-chevron-right"></i></a>
                        </div><!--//section-content-->
                    </section><!--//events-->