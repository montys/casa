
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Casa de Montessori</title>

<?php
$page="home";
$homealert;
require_once("./jeanneEdit/alerts.php");
require_once("./inc/meta.php");
?>

<meta name="google-site-verification" content="U-P1i0awbnjo3igUWkgALinhnls3YgKvCdQPW2LPxy4" />

</head> 

<body class="home-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

        <!-- ******CONTENT****** --> 
        <div class="content container">

				<?php
					checkAlert($homealert);
        ?>

            <div id="promo-slider" class="slider flexslider">
                <ul class="slides">
                    <!--li>
                        <img src="assets/images/slides/72_Open_House.jpg"  alt="" />
                    </li>									
                    <li>
                        <img src="assets/images/Open_House_Promo_2019.jpg"  alt="" />
                    </li-->
  									<li>
                        <img src="assets/images/slides/slide-0.png"  alt="" />
                    </li>
                    <li>
                        <img src="assets/images/slides/slide-1a.jpg"  alt="" />
                        <p class="flex-caption">
                            <span class="main" >Join Casa de Montessori</span>
                            <br />
                            <span class="secondary clearfix" ><i>Free the child’s potential, and you will transform him into the world</i> – Dr. Maria Montessori.</span>
                        </p>
                    </li>
                    <li>
                        <img src="assets/images/slides/slide-2a.jpg"  alt="" />
                        <p class="flex-caption">
                            <span class="main" >A love of learning</span>
                            <br />
                            <span class="secondary clearfix" >Our role is to preserve and nurture your child’s natural curiosity.</span>
                        </p>
                    </li>
                    <li>
                        <img src="assets/images/slides/slide-3.jpg"  alt="" />
                        <p class="flex-caption">
                            <span class="main" >The important early formative years</span>
                            <br />
                            <span class="secondary clearfix" >The child's development follows a path of successive stages of independence.</span>
                        </p>
                    </li>
                    <li>
                        <img src="assets/images/slides/slide-5.jpg"  alt="" />
                        <p class="flex-caption">
                            <span class="main" >A structured educational environment</span>
                            <br />
                            <span class="secondary clearfix" >Children are carefully nurtured so that they are able to develop to their fullest potential.</span>
                        </p>
                    </li>
                    <li>
                        <img src="assets/images/slides/slide-5a.jpg"  alt="" />
                        <p class="flex-caption">
                            <span class="main" >A dynamic, energized classroom</span>
                            <br />
                            <span class="secondary clearfix" >Children learn best when they are actively involved in the learning process.</span>
                        </p>
                    </li>
                </ul><!--//slides-->
            </div><!--//flexslider-->
            <section class="promo box box-dark">        
                <div class="col-md-9">
                <h1 class="section-heading">Casa de Montessori - since 1969</h1>
                    <p>Casa de Montessori has instilled a love of learning since its inception in 1969.  During the course of those years, only two people have headed the school and we have experienced incredibly low turnover with our faculty and staff – because the caring and nurturing environment that works for our students also works for our staff.</p>   
                </div>  
                <div class="col-md-3">
                    <a class="btn btn-cta" href="programs.php"><i class="fa fa-play-circle"></i>Our Programs</a>  
                </div>
            </section><!--//promo-->
            <section class="news">
                <h1 class="section-heading text-highlight"><span class="line">Overview</span></h1>     
                <div class="carousel-controls">
                    <a class="prev" href="#news-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                    <a class="next" href="#news-carousel" data-slide="next"><i class="fa fa-caret-right"></i> </a>
                </div><!--//carousel-controls--> 
                <div class="section-content clearfix">
                    <div id="news-carousel" class="news-carousel carousel slide">
                        <div class="carousel-inner">
                            <div class="item active"> 
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="about.php">Highly experienced teachers</a></h2>
                                    <img class="thumb" src="assets/images/news/casa-news-1.jpg"  alt="" />
                                    <p>Our teaching staff have passed rigorous training, hold bachelor's degrees and are AMI certified.</p>
                                    <a class="read-more" href="about.php">Read more<i class="fa fa-chevron-right"></i></a>                
                                </div><!--//news-item-->
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="typical-day.php">The classroom experience</a></h2>
                                    <p>Teachers tailor the classroom experience to each child's individual development needs, allowing them to develop at their own pace.</p>
                                    <a class="read-more" href="typical-day.php">Read more<i class="fa fa-chevron-right"></i></a>
                                    <img class="thumb" src="assets/images/news/casa-news-3.jpg"  alt="" />
                                </div><!--//news-item-->
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="programs.php">Mixture of ages</a></h2>
                                    <p>The mixture of ages allows children to learn from each other, which enhances social and intellectual development, and fosters greater interest and cooperation.</p>
                                    <a class="read-more" href="programs.php">Read more<i class="fa fa-chevron-right"></i></a>
                                    <img class="thumb" src="assets/images/news/casa-news-2.jpg"  alt="" />
                                </div><!--//news-item-->
                            </div><!--//item-->
                            <div class="item"> 
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="programs.php">Learn</a></h2>
                                    <img class="thumb" src="assets/images/news/casa-news-4.jpg"  alt="" />
                                    <p>Our programs focus on five key learning areas: practical life, sensorial activities, language, math and culture.</p>
                                    <a class="read-more" href="programs.php">Read more<i class="fa fa-chevron-right"></i></a>                
                                </div><!--//news-item-->
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="programs.php">Discover</a></h2>
                                    <p>Each class has specially designed materials to engage and help improve fine motor skills as well as hand-eye coordination.</p>
                                    <a class="read-more" href="programs.php">Read more<i class="fa fa-chevron-right"></i></a>
                                    <img class="thumb" src="assets/images/news/casa-news-5.jpg"  alt="" />
                                </div><!--//news-item-->
                                <div class="col-md-4 news-item">
                                    <h2 class="title"><a href="programs.php">Explore</a></h2>
                                    <p>The two classrooms are wonderfully spacious with tall windows and natural light. Outside we have access to 6 acres of grass fields and playground.</p>
                                    <a class="read-more" href="programs.php">Read more<i class="fa fa-chevron-right"></i></a>
                                    <img class="thumb" src="assets/images/news/casa-news-6.jpg"  alt="" />
                                </div><!--//news-item-->
                            </div><!--//item-->
                        </div><!--//carousel-inner-->
                    </div><!--//news-carousel-->  
                </div><!--//section-content-->     
            </section><!--//news-->
            <div class="row cols-wrapper">
                <div class="col-md-3">
                    <section class="links">
                        <h1 class="section-heading text-highlight"><span class="line">Term Dates</span></h1>
                        <div class="section-content">
                            <ul class="list-unstyled">
                                <li><i class="fa fa-caret-right"></i> September 4, 2018 to</li>
                                <li><i class="fa fa-caret-right"></i> June 6, 2019</li>
                            </ul>
                        </div>
                    </section>

                    <?php
                      require_once("./inc/holidays.php");
                    ?>
                    
                </div><!--//col-md-3-->
                <div class="col-md-6">
                    <section class="video">
                        <h1 class="section-heading text-highlight"><span class="line">Photo Gallery</span></h1>
                        <div class="carousel-controls">
                            <a class="prev" href="#videos-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                            <a class="next" href="#videos-carousel" data-slide="next"><i class="fa fa-caret-right"></i> </a>
                        </div><!--//carousel-controls-->
                        <div class="section-content">    
                           <div id="videos-carousel" class="videos-carousel carousel slide">
                                <div class="carousel-inner">

                    <?php
											// Define the collection of slides to include [general | graduation]
                      require_once("./inc/home-slides-general.php");
                    ?>

                                </div><!--//carousel-inner-->                                
                           </div><!--//videos-carousel-->                            
                            <p class="description">Casa in action!</p>
                        </div><!--//section-content-->
                    </section><!--//video-->
                </div>
                <div class="col-md-3">

                    <?php
                      require_once("./inc/forms.php");
                    ?>

                    <section class="testimonials">
                        <h1 class="section-heading text-highlight"><span class="line"> Testimonials</span></h1>
                        <div class="carousel-controls">
                            <a class="prev" href="#testimonials-carousel" data-slide="prev"><i class="fa fa-caret-left"></i></a>
                            <a class="next" href="#testimonials-carousel" data-slide="next"><i class="fa fa-caret-right"></i> </a>
                        </div><!--//carousel-controls-->
                        <div class="section-content">
                            <div id="testimonials-carousel" class="testimonials-carousel carousel slide">
                                <div class="carousel-inner">
                                    <div class="item active">
                                        <blockquote class="quote">                                  
                                            <p><i class="fa fa-quote-left"></i>My child attends Casa now and I think it's been a great atmosphere and wonderful experience for him. He really enjoys his time there and we have been quite happy with our choice. I highly recommend it.</p>
                                        </blockquote>                
                                    </div><!--//item-->
                                    <div class="item">
                                        <blockquote class="quote">
                                            <p><i class="fa fa-quote-left"></i>
                                            We love that the teachers have been there for years and really know how to inspire the child. Mine love being at school where they have learned about Galileo and the stars through to planting tulip bulbs and butterflies.</p>
                                        </blockquote>
                                    </div><!--//item-->
                                    <div class="item">
                                        <blockquote class="quote">
                                            <p><i class="fa fa-quote-left"></i>
                                            We absolutely LOVE the school. We have been a part of its community for 7 years. It is truly a special place. My children consider school a happy, safe place they like to be. We love that the Montessori approach meets the child where they are.</p>
                                        </blockquote>
                                    </div><!--//item-->
                                </div><!--//carousel-inner-->
                            </div><!--//testimonials-carousel-->
                        </div><!--//section-content-->
                    </section><!--//testimonials-->
                </div><!--//col-md-3-->
            </div><!--//cols-wrapper-->
        </div><!--//content-->

    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>
 
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>            


</body>
</html> 

