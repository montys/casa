<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Apply</title>

<?php
$page="apply";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Apply</h1>
                </header> 
                <div class="page-content">
                    <div class="row page-row">
                        <div class="course-wrapper col-md-8 col-sm-7">                         
                            <article class="">

                                <p class="featured-image page-row"><img class="img-responsive" src="assets/images/courses/course-2.jpg" alt=""/></p>
                                
                                <p>Thank you for your interest in Casa de Montessori.  We look forward to answering any questions you might have or helping you with the application process.</p>
                                

                                <h2>Application Process</h2>
                                <ul>
                                    <li>An appointment is made to observe in the classroom. The parent is advised if there is a waiting list.</li>
                                    <li>An application is given upon request after observing.</li>
                                    <li>A child is considered enrolled or on a waiting list when the signed and completed application is submitted with the non-refundable application fee and tuition deposit.</li>
                                    <li>Children can be admitted during the school year if there is an opening.</li>
                                </ul>
                                <h2>Tuition Policy</h2>
                                <ul>
                                    <li>Students are enrolled for the entire school year ending in June.</li>
                                    <li>Tuition payments may be made on a monthly installment basis on the first of each month beginning August 1st.</li>
                                    <li>Tuition is reduced by 10% for the oldest sibling when more than 1 child in a family is enrolled.</li>
                                    <li>Tuition is prorated for children enrolling later in the school year.</li>
                                    <li>A 5% late fee will be charged for tuition payments made more than one week late.</li>
                                    <li>A $10 fee will be charged for any check returned to the school for non-sufficient funds.</li>
                                    <li>No tuition adjustments will be made for any absence including vacations.</li>
                                </ul>                                    
                                <h2>2015/2016 School Year Tuition Information</h2>
                                <ul>
                                    <li>Application Fee – applies to every child enrolled – Non-refundable one time fee - $ 100</li>
                                    <li>Pre-school Program – Monday through Friday, 8:45 AM to 12:00 PM - $ 6,000</li>
                                    <li>Primary All Day Program – Monday through Friday, 8:45 AM to 3:00P PM – $8,600</li>
                                    <li>Kindergarten – Monday through Friday, 8:45 AM to 3:00 PM - $ 8,600</li>
                                </ul>
                                <h2>Refund Policy</h2>
                                <p>All prepaid tuition (excluding non-refundable tuition deposit) is refunded when a student withdraws before having attended Casa. Otherwise, the school must be notified 60 days prior to any withdrawal for any refundable tuition to apply. Tuition must be paid through the end of the actual month in which the student withdraws.</p>
                            </article><!--//course-item-->                                              
                        </div><!--//course-wrapper-->
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">     

                            <?php
                            require_once("./inc/brochure.php");
                            ?>  

                            <?php
                            require_once("./inc/forms.php");
                            ?>

                            <?php
                            require_once("./inc/referral.php");
                            ?>

                        </aside>
                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page--> 
        </div><!--//content-->
    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script> 
    
</body>
</html> 

