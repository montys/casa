<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
<head>
    <title>Contact Us</title>

<?php
$page="contact";
require_once("./inc/meta.php");
?>

</head> 

<body class="inner-page">
    <div class="wrapper">

<?php
require_once("./inc/header.php");
?>

    
        <!-- ******CONTENT****** --> 
        <div class="content container">
            <div class="page-wrapper">
                <header class="page-heading clearfix">
                    <h1 class="heading-title pull-left">Contact</h1>
                </header> 
                <div class="page-content">
                    <div class="row">
                        <article class="contact-form col-md-8 col-sm-7  page-row">
                        
													<p>If you wish to get in touch, please email us at:</p>
                          <p><a href="mailto:info@casademontessori.com">Email: info@casademontessori.com</a></p>
  
  												<br />
  
                          <h2 class="title">How to find us</h2>
                          <p>Casa is conveniently located on New Hampshire Avenue 1/2 mile north of Randolph Road and 1/8 mile south of the Intercounty Connector.</p>
                          <p><i class="fa fa-exclamation-circle"></i> <strong>Please note:</strong> we are located in the <strong>back wing</strong> of the former Colesville Elementary School campus, with direct access to the spacious 6 acre park and playground behind the school.</p>
                          <div id="map"></div>
                          <a href="https://www.google.com/maps/place/Casa+de+Montessori/@39.0860837,-76.9986016,17z/data=!4m2!3m1!1s0x0000000000000000:0x6596c58252cd1677">Bigger Map</a><!--//map-->
                                                                     
                        </article><!--//contact-form-->
                        
                        <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">

                            <?php
                            require_once("./inc/brochure.php");
                            ?> 
                            
                            <section class="widget has-divider">
                                <h3 class="title">Postal Address</h3>
                                <p class="adr">
                                    <span class="adr-group">       
                                        <span class="street-address">14015 New Hampshire Ave</span><br>
                                        <span class="region">Silver Spring, MD</span><br>
                                        <span class="postal-code">20904</span><br>
                                        <span class="country-name">USA</span>
                                    </span>
                                </p>
                            </section><!--//widget-->     
                              
                        </aside><!--//page-sidebar-->
                    </div><!--//page-row-->
                    <div class="page-row">

                    </div><!--//page-row-->
                </div><!--//page-content-->
            </div><!--//page-wrapper--> 
        </div><!--//content-->
    </div><!--//wrapper-->

<?php
require_once("./inc/footer.php");
?>
    
    <!-- Javascript -->          
    <script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/bootstrap-hover-dropdown.min.js"></script> 
    <script type="text/javascript" src="assets/plugins/back-to-top.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery-placeholder/jquery.placeholder.js"></script>
    <script type="text/javascript" src="assets/plugins/pretty-photo/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="assets/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="assets/plugins/jflickrfeed/jflickrfeed.min.js"></script> 
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="assets/plugins/gmaps/gmaps.js"></script>   
    <script type="text/javascript" src="assets/js/map.js"></script> 
    <script type="text/javascript" src="assets/js/main.js"></script>          
    <script type="text/javascript" src="assets/js/min/jquery.validate.min.js"></script>
    
		<script>
    $("#contactForm").validate();
    </script>
    
</body>
</html> 

